import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.services';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent  implements OnInit {
  loginForm: FormGroup;

  constructor(private authService: AuthService, private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: [''],
      password: ['']
    });
  }

  // get f() { return this.loginForm.controls; }
  //
  // login() {
  //   this.authService.login(
  //     {
  //       username: this.f.username.value,
  //       password: this.f.password.value
  //     }
  //   )
  //     .subscribe(success => {
  //       if (success) {
  //         this.router.navigate(['/dashboard']);
  //       }
  //     });
  // }
}
